<?php

namespace Drupal\custom_dev\Plugin\Block;

use Drupal\Core\Block\BlockBase;
/**
 * To add a form to Block configuration use below namespaces
 */
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide a custom block
 *
 * @Block(
 *   id = "custom_dev_block",
 *   admin_label = @Translation("Custom Dev Block"),
 *   category = @Translation("Custom Development Block"),
 * )
 */
 
class CustomDevBlock extends BlockBase implements BlockPluginInterface {
	/**
	 * {@inheridoc}
	 * The above doc is just inherited from the extended BlockBase class
	 */
	public function build() {
		$config = $this->getConfiguration();
		
		if(!empty($config['hello_block_name'])) {
	      $name = $config['hello_block_name'];
		}
		else{
		  $name = $this->t('no one');
		}
		return array(
		 '#markup' => $this->t('Here is our custom block from @name', array('@name' => $name))
		);
	}
	
	/**
	 * {@inheridoc}
	 */
	public function defaultConfiguration() {
	  $default_config = \Drupal::config('custom_dev.settings');
	  return [
	    'hello_block_name' => $default_config->get('mysettings.name')
	  ];
	}
	/**
	 * {@inheridoc}
	 */
	public function blockForm($form, FormStateInterface $form_state) {
      $form = parent::blockForm($form, $form_state);
	  
	  $config = $this->getConfiguration();

      $form['hello_block_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Who'),
        '#description' => $this->t('Who do you want to say hello to?'),
        '#default_value' => isset($config['hello_block_name']) ? $config['hello_block_name'] : '',
      ];

      return $form;
	}
	/**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
      parent::blockSubmit($form, $form_state);
      $values = $form_state->getValues();
      $this->configuration['hello_block_name'] = $values['hello_block_name'];
    }
}