<?php
/**
 * @file
 * Contains \Drupal\custom_dev\Form\CustomForm.
 */
namespace Drupal\custom_dev\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements an custom form.
 */
class CustomForm extends FormBase {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Class constructor.
   */
  public function __construct(AccountInterface $account) {
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'custom_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get current user data.
    $uid = $this->account->id();
    drupal_set_message($uid);
	
    $form['user_info'] = [
	  '#type' => 'markup',
      '#markup' => $this->t('Your User ID is %uid', array('%uid' => $uid)),

	];
	$form['your_name'] = [
	  '#type' => 'textfield',
      '#title' => $this->t('Your Name'),

	];
	
	return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // ...
  }

}
